use gtk::prelude::*;
use gtk::Application;
use tor_bridges_manager::tor_insert;

pub fn init_gui() {
    let app = Application::builder()
        .application_id("com.typalyapteam.tor_bridge_manager_gui")
        .build();

    app.connect_activate(|app| {
        let glade_src = include_str!("view.glade");
        let builder = gtk::Builder::from_string(glade_src);
        let window: gtk::Window = builder.object("window").unwrap();
        window.set_application(Some(app));

        let new_bridges_text: gtk::TextView = builder.object("new_bridges_text").unwrap();
        let change_bridges_button: gtk::Button = builder.object("change_bridges_button").unwrap();
        let compile_btn: gtk::Button = builder.object("compile_btn").unwrap();
        let compiled_text: gtk::Label = builder.object("compiled_text").unwrap();
        let old_bridges_text: gtk::Label = builder.object("old_bridges_text").unwrap();
        let save_old_bridges_checkbox: gtk::Switch =
            builder.object("save_old_bridges_checkbox").unwrap();
        let old_file = tor_insert::read_file("/etc/tor/torrc").unwrap_or("".to_string());
        old_bridges_text.set_text(&{ tor_insert::get_bridges(&old_file).as_str() });
        let old_file = tor_insert::clear_bridges(&old_file);
        {
            let compiled_text = compiled_text.clone();
            compile_btn.connect_clicked(move |_button| {
                let new_bridges_text = &new_bridges_text.buffer().unwrap();
                let (start_bridges, end_bridges) = new_bridges_text.bounds();
                let new_bridges_text = new_bridges_text
                    .text(&start_bridges, &end_bridges, true)
                    .unwrap()
                    .to_string();
                let _ = &compiled_text.set_text(&match save_old_bridges_checkbox.is_active() {
                    true => format!("{}\n{}", old_bridges_text.text(), new_bridges_text),
                    false => new_bridges_text,
                });
            });
        }

        {
            let compiled_text = compiled_text.clone();
            change_bridges_button.connect_clicked(move |_| {
                print!("File = {}", &old_file);
                let compiled_text = compiled_text.text().to_string();
                let new_file = tor_insert::add_new_bridges(&old_file, &compiled_text);
                match tor_insert::write_file(new_file.clone(), "/etc/tor/torrc") {
                    Ok(_) => (),
                    Err(err) => {
                        eprintln!("Error: {}", err);
                        tor_insert::write_file(new_file, "torrc").unwrap()
                    }
                };
            });
        }

        window.show_all();
    });

    app.run();
}
